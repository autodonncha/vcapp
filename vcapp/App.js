/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, Button, Alert} from 'react-native';

export default class CodeInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
      balance: '',
  };

    checkVoucher = (event) => {
      Alert.alert('Voucher is Valid');
      this.setState({
        balance: '€150'
      });
    }    
  }


  render() {
    return (
      <View style={{padding:10}}>
        <Text style={{padding: 10, fontSize: 34}}>Voucher Connect</Text>
      <TextInput
      style={{padding: 10, height: 40}}
      autoCorrect={false}
      placeholder="Enter Voucher Code"
      onChangeText={(code) => this.setState({code})}
      />
      <Text style={{padding: 10, fontSize: 30}}>
        {this.state.balance}
      </Text>
      <Button
        onPress={() => {
          // Alert.alert('Voucher is Valid');
          checkVoucher();
        }}
        title="Check"
      />
      </View>
    );
  }
}
